<?php
 
if(isset($_POST['email_field'])) {
 
     
 
 
    $email_to = "connect@indywebco.com";
 
    $email_subject = "[REFERRAL SUBMISSION]";
 
     
 
     
 
    function died($error) {
 
header('Location: /oops.html'); 
        die();
 
    }
 
     
 
    // validation expected data exists
 
    if(!isset($_POST['name']) ||
 
        !isset($_POST['email_field']) ||
 
        !isset($_POST['phone']) ||
 
        !isset($_POST['business']) ||
 
        !isset($_POST['website'])) {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
     
 
    $first_name = $_POST['name']; // required
 
    $email_from = $_POST['email_field']; // required
 
    $telephone = $_POST['phone']; // not required

    $purpose = $_POST['business']; // not required

    $amount = $_POST['website']; // not required
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
 
    $email_message = "Referral Details Below.\n\n";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
     
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
 
    $email_message .= "Email: ".clean_string($email_from)."\n";
 
    $email_message .= "Telephone: ".clean_string($telephone)."\n";
 
    $email_message .= "Business: ".clean_string($purpose)."\n";
 
    $email_message .= "Website: ".clean_string($amount)."\n";
 
 
     
 
     
 
// create email headers
 
$headers = 'From: '.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
header('Location: /thank-you.html'); 
?>
 
 
 
<!-- include your own success html here -->
 
 
 
Thank you for contacting us. We will be in touch with you very soon.
 
 
 
<?php
 
}
 
?>
